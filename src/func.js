const getSum = (str1, str2) => {
  // add your implementation below
  let number1 = parseInt(str1);
  let number2 = parseInt(str2);
  let result;
  if (!isNaN(number1) || !isNaN(number2)) {
    result = 0;
    if (!isNaN(number1)) {
      result += number1;
    }
    if (!isNaN(number2)) {
      result += number2;
    }
    return result.toString();
  }
  else {
    return false;
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  // add your implementation below
  let amountPosts = 0;
  let amountComments = 0;
  for (const post of listOfPosts) {
    if (post.author === authorName) {
      amountPosts++;
    }
    if (post.comments != undefined) {
      for (const comment of post.comments) {
        if (comment.author === authorName) {
          amountComments++;
        }
      }
    }
  }
  return `Post:${amountPosts},comments:${amountComments}`;
};

const tickets = (people) => {
  // add your implementation below
  const price = 25;
  let money = [];
  for (const entry of people) {
    let duty = entry - price;

    for (let i = 0; i < money.length; i++) {
      if (duty >= money[i]) {
        duty -= money[i];
        money.splice(i, 1);
      }
    }

    if (duty > 0) {
      return 'NO'
    }

    money.push(entry);
    money.sort((a, b) => b - a)
  }

  return 'YES';
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
